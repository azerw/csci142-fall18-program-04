package vc;

import javax.swing.JOptionPane;

import connectmodel.*;

public class Controller
{
    private View                    myView;
    private connectmodel.GameEngine myModel;
    private connectmodel.Player     myPlayer;
    private connectmodel.GameBoard  myGameBoard;
    private PieceType[]             myPieces = new PieceType[2];

    /**
     * Constructor of Controller. Create the Player and GameBoard once the
     * player's name and piece are entered.
     */
    public Controller()
    {

        myView = new View(this);
        switch (myView.getPiece())
        {
            case "red" :
                myPieces[0] = PieceType.RED;
                myPieces[1] = PieceType.BLACK;
                break;
            case "black" :
                myPieces[0] = PieceType.BLACK;
                myPieces[1] = PieceType.RED;
                break;
            case "green" :
                myPieces[0] = PieceType.GREEN;
                myPieces[1] = PieceType.YELLOW;
                break;
            case "yellow" :
                myPieces[0] = PieceType.YELLOW;
                myPieces[1] = PieceType.GREEN;
                break;
        }
        myPlayer = new Player(myView.getMyName(), myPieces[0]);
        myGameBoard = new GameBoard(6, 7, 4, myPieces);
        myModel = new GameEngine(myPlayer, myGameBoard);
    }

    /**
     * Place a piece when player click the game board and generate a massage in
     * the dash board. The computer will take its turn automatically. Check if
     * the game is win every piece placed.
     * 
     * @param iRow
     */ 
    public void placePiece(Integer iRow)
    {
        int col, row;
        col = iRow.intValue();
        if (myModel.placePiece(col) == true)
        {
            
            row = myModel.getGameBoard().getLastPoint().y;
            myView.changeImage(col, row, myModel.getPlayerUp().getName());
            myView.setMyMessage(myModel.getPlayerUp().getName() + " "
                    + "placed a piece at: " + row +"," +col + "\r\n");
            if (myModel.getGameBoard().checkIfWin() == true)
            {
                String message;
                if (myModel.getPlayerUp().getName() == myView.getMyName())
                {
                    message = "You win!";
                    myModel.getPlayerUp().incrementScore();
                    myView.setPlayerScore("" + this.getScore());
                    myView.setMyMessage(myModel.getPlayerUp().getName() + " "
                            + "wins!" + "\r\n");
                } else
                {
                    message = "You lose!";
                    myModel.getPlayerUp().incrementScore();
                    myView.setComputerScore("" + this.getScore());
                    myView.setMyMessage("Computer " + "wins!" + "\r\n");
                }

                Object[] options =
                {"Play again", "Exit"};
                int response = JOptionPane.showOptionDialog(null, message, null,
                        JOptionPane.YES_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                        options, options[0]);

                switch (response)
                {
                    case 0 :
                        myView.start();
                        this.start();
                        break;

                    case 1 :
                        myView.closeWindow();
                        break;
                }
            } else
            {
                myModel.switchPlayerUp();

                if (myModel.getPlayerUp().getName() == "Computer")
                {
                    this.computerTurn();
                }
            }
            if (myModel.getGameBoard().isBoardFull() == true)
            {
                Object[] options =
                {"Play again", "Exit"};
                int response = JOptionPane.showOptionDialog(null,
                        "You're tied!", null, JOptionPane.YES_OPTION,
                        JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

                switch (response)
                {
                    case 0 :
                        myView.start();
                        this.start();
                        break;

                    case 1 :
                        myView.closeWindow();
                        break;
                }
            }
        } else
        {
            myView.fullColumn();
        }
    }

    /**
     * Start the game.
     */
    public void start()
    {
        myModel.startGame();
        if (myModel.getPlayerUp().getName() == "Computer")
        {
            this.computerTurn();
        }
    }

    /**
     * Record the score in the game information part.
     * 
     * @return score
     */
    public String getScore()
    {
        String score;
        score = "" + myModel.getPlayerUp().getNumWins();
        return score;
    }

    /**
     * Let computer player place a piece.
     */
    public void computerTurn()
    {
        int column = myGameBoard.findComputerbestMove();
        this.placePiece(column);
    }

}
