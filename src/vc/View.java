package vc;
/**
 * Simple view class used to show how Model-View-Controller
 * is implemented.  This is also the main application.
 * Version 2.0 uses Generics.
 *
 * @author Daniel Plante
 * @version 1.0 (28 January 2002)
 * @version 2.0 (1 February 2008)
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.*;

import javax.swing.*;

public class View
{
    //////////////////////
    // Properties //
    //////////////////////

    private Can[][]           mySquare;
    private JPanel            mySquaresPanel, myCompetitors, myDisplay;
    private ButtonListeners[] mySquareListener;
    private JTextField        myPlayerScore, myComputerScore;
    private JTextArea         myDashBoard;
    private Controller        myController;
    private Image             myBlankImage;
    private String            myName, myPiece, myComputer;
    private Image             myPlayerPiece;
    private Image             myComputerPiece;
    private JFrame            myFrame;

    ///////////////////////
    // Methods //
    ///////////////////////
    /**
     * View constructor used to lay out the view
     *
     * <pre>
     * pre:  none
     * post: the view is set up and initialized
     * </pre>
     */
    public View(Controller controller)
    {
        int i;

        myFrame = new JFrame("Connect 4");

        myFrame.setSize(875, 800);
        myFrame.setLayout(null);
        myFrame.setBackground(Color.gray);

        this.setName();
        this.setPiece();

        myBlankImage = Toolkit.getDefaultToolkit().getImage("images/blank.jpg");

        mySquare = new Can[6][7];

        mySquareListener = new ButtonListeners[7];
        mySquaresPanel = new JPanel(new GridLayout(6, 7));
        mySquaresPanel.setSize(574, 498);
        mySquaresPanel.setLocation(260, 80);

        for (int r = 0; r < 6; r++)
        {
            for (i = 0; i < 7; i++)
            {
                mySquare[r][i] = new Can(myBlankImage);
                mySquaresPanel.add(mySquare[r][i]);
            }
        }
        myController = controller;
        
        myCompetitors = new JPanel();
        myCompetitors.setSize(210, 500);
        myPlayerScore = new JTextField("0");
        myComputerScore = new JTextField("0");
        myCompetitors.setLayout(new GridLayout(6, 2));
        myCompetitors.add(new JLabel("Player Name: "));
        myCompetitors.add(new JLabel(myName));
        myCompetitors.add(new JLabel("Piece: "));
        myCompetitors.add(new JLabel(myPiece));
        myCompetitors.add(new JLabel("Score: "));
        myCompetitors.add(myPlayerScore);
        myCompetitors.add(new JLabel("Player Name: "));
        myCompetitors.add(new JLabel("Computer"));
        myCompetitors.add(new JLabel("Piece: "));
        myCompetitors.add(new JLabel(myComputer));
        myCompetitors.add(new JLabel("Score: "));
        myCompetitors.add(myComputerScore);

        myDisplay = new JPanel();
        myDisplay.setSize(210, 600);
        myDisplay.setLayout(new BoxLayout(myDisplay, BoxLayout.Y_AXIS));
        myDisplay.setLocation(20, 20);
        
        myDashBoard = new JTextArea(10,1);
        myDashBoard.setLineWrap(true);
        myDashBoard.setWrapStyleWord(true);
        
        JScrollPane jsp = new JScrollPane(myDashBoard);
        jsp.setBounds(0, 0, 100, 400);
        
        myDashBoard.setSize(100, 400);
        myDashBoard.append("Start Game!"+"\r\n");
        myDisplay.add(myCompetitors);
        myDisplay.add(jsp);
        JButton start = new JButton("Start");
        myDisplay.add(start);
        start.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                for (int r = 0; r < 6; r++)
                {
                    for (int i = 0; i < 7; i++)
                    {
                        mySquare[r][i].setImage(myBlankImage);
                    }
                }
                myDashBoard.append("Start game!"+"\r\n");
                
                myController.start();
            }
        });

        myFrame.add(mySquaresPanel);
        myFrame.add(myDisplay);

        myFrame.setVisible(true);
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.associateListeners();
    }
    /**
     * Get the player's name.
     */
    public void setName()
    {
        myName = JOptionPane.showInputDialog("Enter your name: ");
    }

    public String getMyName()
    {
        return myName;
    }

    /**
     * Let player choose their piece
     */
    public void setPiece()
    {
        String[] options =
        {"Red", "Black", "Yellow", "Green"};
        int response = JOptionPane.showOptionDialog(null,
                "Select your piece:\n", "Piece type", JOptionPane.YES_OPTION,
                JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

        switch (response)
        {
            case 0 :
                myPlayerPiece = Toolkit.getDefaultToolkit()
                        .getImage("images/red.jpg");
                myComputerPiece = Toolkit.getDefaultToolkit()
                        .getImage("images/black.jpg");
                myPiece = "red";
                myComputer = "black";

                break;
            case 1 :
                myPlayerPiece = Toolkit.getDefaultToolkit()
                        .getImage("images/black.jpg");
                myComputerPiece = Toolkit.getDefaultToolkit()
                        .getImage("images/red.jpg");
                myPiece = "black";
                myComputer = "red";
                break;
            case 2 :
                myPlayerPiece = Toolkit.getDefaultToolkit()
                        .getImage("images/yellow.jpg");
                myComputerPiece = Toolkit.getDefaultToolkit()
                        .getImage("images/green.jpg");
                myPiece = "yellow";
                myComputer = "green";
                break;
            case 3 :
                myPlayerPiece = Toolkit.getDefaultToolkit()
                        .getImage("images/green.jpg");
                myComputerPiece = Toolkit.getDefaultToolkit()
                        .getImage("images/yellow.jpg");
                myPiece = "green";
                myComputer = "yellow";
                break;
        }
    }

    public String getPiece()
    {
        return myPiece;
    }

    /**
     * Associates each component's listener with the controller and the correct
     * method to invoke when triggered.
     *
     * <pre>
     * pre:  the controller class has be instantiated
     * post: all listeners have been associated to the controller
     *       and the method it must invoke
     * </pre>
     */
    public void associateListeners()
    {
        Class<? extends Controller> controllerClass;
        Method incrementMethod;
        Class<?>[] classArgs;

        controllerClass = myController.getClass();

        incrementMethod = null;
        classArgs = new Class[1];

        try
        {
            classArgs[0] = Class.forName("java.lang.Integer");
        } catch (ClassNotFoundException e)
        {
            String error;
            error = e.toString();
            System.out.println(error);
        }
        try
        {
            incrementMethod = controllerClass.getMethod("placePiece",
                    classArgs);
        } catch (NoSuchMethodException exception)
        {
            String error;

            error = exception.toString();
            System.out.println(error);
        } catch (SecurityException exception)
        {
            String error;

            error = exception.toString();
            System.out.println(error);
        }

        int i;
        Integer[] args;

        for (int c = 0; c < 7; c++)
        {
            args = new Integer[1];
            args[0] = c;
            mySquareListener[c] = new ButtonListeners(myController,
                    incrementMethod, args);
            for (i = 5; i > -1; i--)
            {
                mySquare[i][c].addMouseListener(mySquareListener[c]);
            }
        }
    }

    /**
     * Change the image after the player placed a piece.
     * @param col
     * @param row
     * @param playerUp
     */
    public void changeImage(int col, int row, String playerUp)
    {
        if (playerUp == "Computer")
        {
            mySquare[row][col].setImage(myComputerPiece);
        } else
        {
            mySquare[row][col].setImage(myPlayerPiece);
        }
    }
    
    /**
     * Restart the game. Generate a message in the dash board.
     */
    public void start() {
        for (int r = 0; r < 6; r++)
        {
            for (int i = 0; i < 7; i++)
            {
                mySquare[r][i].setImage(myBlankImage);
            }
        }
        myDashBoard.append("Start game!"+"\r\n");
    }
  
    /**
     * Close the window
     */
    public void closeWindow() {
        myFrame.dispose(); 
    }
    
    /**
     * Show a message when the column is full.
     */
    public void fullColumn() {
        JOptionPane.showMessageDialog(null, "Cannot place here!","Warning",JOptionPane.WARNING_MESSAGE);
    }
    
    public void setPlayerScore(String text) {
        myPlayerScore.setText(text);
    }
    
    public void setComputerScore(String text) {
        myComputerScore.setText(text);
    }
    
    public void setMyMessage(String text) {
        myDashBoard.append(text);
    }

}