package vc;

public class MVC_Basics
{
    // Properties
    private Controller myController;
    
    // Methods
    public static void main(String[] args)
    {
        new MVC_Basics();
    }
    
    public MVC_Basics()
    {
        setController(new Controller());
    }

    public void setController(Controller controller) 
    {
        myController = controller;
    }

    public Controller getController() {
        return myController;
    }

}
