package connectmodel;

/**
 * Piece type numeration
 * @author Yizheng Wang
 *
 */
public enum PieceType 
{
    RED ("Red"),
    BLACK ("Black"),
    GREEN ("Green"),
    YELLOW ("Yellow");
    
    private final String myType;
    
    private PieceType(String type) 
    {
        myType = type;
    }
    
    public String getType()
    {
        return myType;
    }
}