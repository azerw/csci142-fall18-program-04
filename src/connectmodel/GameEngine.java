package connectmodel;

import java.util.Vector;

/**
 * Game engine of connect 4.
 * @author Yizheng Wang
 *
 */
public class GameEngine 
{
    Vector<Player>    myPlayers;
    Player            myPlayerUp;
    private Player    myStartingPlayer;
    private GameBoard myGameBoard;

    /**
     * Constructor of game engine.
     * 
     * @param player
     * @param gameBoard
     */
    public GameEngine(Player player, GameBoard gameBoard)
    {
        myPlayers = new Vector<Player>();
        myPlayers.add(player);
        if (gameBoard != null)
        {
            myPlayers.add(gameBoard.getComputerPlayer());
        }
        myGameBoard = gameBoard;
        myStartingPlayer = myPlayers.firstElement();
        myPlayerUp = myStartingPlayer;
    }

    /**
     * When the player passed is included in "myPlayers", make it the starting
     * player.
     * 
     * @param player
     * @return if you can select starting player or not
     */
    public boolean selectStartingPlayer(Player player)
    {
        if (myPlayers.contains(player))
        {
            myStartingPlayer = player;
            return true;
        }
        return false;
    }

    /**
     * When game board and players are not null, starting a new game
     * 
     * @return if you can start a new game or not
     */
    public boolean startGame()
    {
        if (myGameBoard != null && myPlayers.firstElement() != null)
        {
            myGameBoard.resetBoard();
            for (int i = 0; i < myPlayers.size(); i++)
            {
                if (myStartingPlayer.getName() != myPlayers.elementAt(i)
                        .getName())
                {
                    myStartingPlayer = myPlayers.elementAt(i);
                    myPlayerUp = myStartingPlayer;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Switch the next player of the game
     * 
     * @return next player
     */
    public Player switchPlayerUp()
    {
        for (int i = 0; i < myPlayers.size(); i++)
        {
            if (myPlayerUp.getName() != myPlayers.elementAt(i).getName())
            {
                myPlayerUp = myPlayers.elementAt(i);
                return myPlayerUp;
            }
        }
        return myPlayerUp;
    }

    /**
     * When the column is not full, placing a piece.
     * 
     * @param column
     * @return If you can place the piece or not.
     */
    public boolean placePiece(int column)
    {
        if (myGameBoard.isColumnFull(column) == false)
        {
            myGameBoard.placePiece(column, myPlayerUp.getPieceType());
            return true;
        }
        return false;
    }

    public Player getPlayerUp()
    {
        return myPlayerUp;
    }

    public Player getStartingPlayer()
    {
        return myStartingPlayer;
    }

    public Vector<Player> getPlayers()
    {
        return myPlayers;
    }

    public void setGameBoard(GameBoard gameboard)
    {
        myGameBoard = gameboard;
    }

    public GameBoard getGameBoard()
    {
        return myGameBoard;
    }

}