package connectmodel;

import java.awt.Point;
import connectmodel.*;

import java.util.Vector;

/**
 * Game board of connect 4
 * 
 * @author Yizheng Wang
 *
 */
public class GameBoard
{
    int               myNumRows;
    private int               myNumColumns;
    private PieceType[][]     myBoard;
    private int               myNumTypes;
    private int               myWinLength;
    private Point             myLastPoint = null;
    private Vector<PieceType> myTypes;
    private Point             myWinBegin  = null;
    private Point             myWinEnd    = null;
    private boolean           myIsAWin;
    private ComputerPlayer    myComputer;

    /**
     * Constructor of gameBorad
     * 
     * @param rows
     * @param cols
     * @param winLength
     * @param types
     */
    public GameBoard(int rows, int cols, int winLength, PieceType[] types)
    {
        myNumRows = rows;
        myNumColumns = cols;
        myWinLength = winLength;
        myBoard = new PieceType[myNumRows][myNumColumns];
        myTypes = new Vector<PieceType>();
        for (int i = 0; i < types.length; i++)
        {
            myTypes.add(types[i]);
        }

        myComputer = new ComputerPlayer("Computer", myTypes.lastElement());
    }

    /**
     * Find the best move for computer
     * 
     * @return best move column for computer
     */
    public int findComputerbestMove()
    {
        int col = this.findBestMoveColumn(myComputer.getPieceType());
        return col;
    }

    /**
     * Place piece and convert the it into a point, where x= column, y = row.
     * 
     * @param col
     * @param type
     * @return if you can place the piece or not
     */
    public boolean placePiece(int col, PieceType type)
    {
        if (col < myNumColumns && col > -1)
        {
            for (int i = myNumRows - 1; i > -1; i--)
            {
                if (myBoard[i][col] == null)
                {
                    myBoard[i][col] = type;
                    myLastPoint = new Point(col, i);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * ResetBoard.
     */
    public void resetBoard()
    {
        myBoard = new PieceType[myNumRows][myNumColumns];
    }

    /**
     * Check if there is a win happens
     * 
     * @return win status
     */
    public boolean checkIfWin()
    {
        if (this.checkVerticalWin() == true || this.checkHorizontalWin() == true
                || this.checkDiagonalWin() == true)
        {
            myIsAWin = true;
            myWinEnd = myLastPoint;
            this.findWinBeginPoints();
            return true;
        }
        return false;
    }

    /**
     * Find best move column. Priorities: Win > Block > Meet the win length
     * 
     * @param type
     * @return best move column
     */
    public int findBestMoveColumn(PieceType type)
    {
        int[] result = new int[myNumColumns];
        int max = -1;
        int tempRow = myNumRows;
        int tempCol = myNumColumns;
        PieceType competitor = myTypes.firstElement();

        for (int i = 0; i < myNumColumns; i++)
        {
            result[i] = countHorizontalLengthIfPiecePlaced(i, type)
                    + countVerticalLengthIfPiecePlaced(i, type)
                    + countDiagonalLengthIfPiecePlaced(i, type);
        }

        for (int i = 0; i < result.length; i++)
        {
            if (result[i] > max)
            {
                max = result[i];
                if(this.isColumnFull(i) == false) {
                    tempCol = i;
                    }
            }
        }

        for (int i = 0; i < myNumColumns; i++)
        {
            for (int j = 5; j >= 0; j--)
            {
                if (myBoard[j][i] == null)
                {
                    tempRow = j;
                    j = -1;
                }
            }
            if (tempRow != myNumRows)
            {

                this.placePiece(i, type);
                if (this.checkIfWin() == true)
                {
                    tempCol = i;
                    myBoard[tempRow][i] = null;
                    i = myNumColumns;
                } else
                {
                    myBoard[tempRow][i] = null;
                    this.placePiece(i, competitor);
                    if (this.checkIfWin() == true)
                    {
                        tempCol = i;
                        myBoard[tempRow][i] = null;
                    }
                    myBoard[tempRow][i] = null;
                }
                tempRow = myNumRows;
            }
        }
        return tempCol;
    }

    /**
     * Check if it is a vertical win
     * 
     * @return win status
     */
    private boolean checkVerticalWin()
    {
        switch (myWinLength)
        {
            case 1 :
                if (isBoardFull() == true)
                {
                    return true;
                }
                break;

            case 2 :
                for (int i = 0; i < 2; i++)
                {
                    if (myBoard[1][i] == myBoard[0][i])
                    {
                        return true;
                    }
                }
                break;

            case 4 :
                for (int i = 0; i < myNumColumns; i++)
                {
                    for (int j = myNumRows - 1; j >= 0; j--)
                    {
                        if (j >= myWinLength - 1)
                        {
                            if (myBoard[j][i] == myBoard[j
                                    - (myWinLength - 1)][i]
                                    && myBoard[j][i] != null)
                            {
                                if (myBoard[j - 1][i] == myBoard[j - 2][i]
                                        && myBoard[j - 1][i] != null)
                                {
                                    if (myBoard[j][i] == myBoard[j - 1][i])
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                break;

        }
        return false;
    }

    /**
     * Check it is a horizontal win
     * 
     * @return win status
     */
    private boolean checkHorizontalWin()
    {
        switch (myWinLength)
        {
            case 1 :
                if (isBoardFull() == true)
                {
                    return true;
                }
                break;
            case 2 :
                for (int i = 1; i >= 0; i--)
                {
                    if (myBoard[i][0] == myBoard[i][1] && myBoard[i][0] != null)
                    {
                        return true;
                    }
                }
                break;

            case 4 :
                for (int i = myNumRows - 1; i >= 0; i--)
                {
                    for (int j = 0; j < myNumColumns; j++)
                    {
                        if (j < myWinLength)
                        {
                            if (myBoard[i][j] == myBoard[i][j + myWinLength - 1]
                                    && myBoard[i][j] != null)
                            {
                                if (myBoard[i][j + 1] == myBoard[i][j + 2]
                                        && myBoard[i][j + 1] != null)
                                {
                                    if (myBoard[i][j] == myBoard[i][j + 1])
                                    {
                                        return true;
                                    }
                                }

                            }
                        }
                    }
                }
                break;
        }
        return false;
    }

    /**
     * Check if a diagonal win happens
     * 
     * @return win status
     */
    private boolean checkDiagonalWin()
    {
        switch (myWinLength)
        {
            case 1 :
                if (isBoardFull() == true)
                {
                    return true;
                }
                break;

            case 2 :
                if ((myBoard[1][0] == myBoard[0][1] && myBoard[1][0] != null)
                        || (myBoard[1][1] == myBoard[0][0]
                                && myBoard[1][1] != null))
                {
                    return true;
                }
                break;

            case 4 :
                for (int j = 0; j < myNumColumns; j++)
                {
                    if (j <= myWinLength - 1)
                    {
                        for (int i = myNumRows - 1; i >= 0; i--)
                        {
                            if (i >= myWinLength - 1)
                            {
                                if (myBoard[i][j] == myBoard[i
                                        - (myWinLength - 1)][j + myWinLength
                                                - 1]
                                        && myBoard[i][j] != null)
                                {
                                    if (myBoard[i - 1][j
                                            + 1] == myBoard[i - 2][j + 2]
                                            && myBoard[i - 1][j + 1] != null)
                                    {
                                        if (myBoard[i][j] == myBoard[i - 1][j
                                                + 1])
                                        {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for (int j = myNumColumns - 1; j >= 0; j--)
                {
                    if (j >= myWinLength - 1)
                    {
                        for (int i = myNumRows - 1; i >= 0; i--)
                        {
                            if (i >= myWinLength - 1)
                            {
                                if (myBoard[i][j] == myBoard[i
                                        - (myWinLength - 1)][j
                                                - (myWinLength - 1)]
                                        && myBoard[i][j] != null)
                                {
                                    if (myBoard[i - 1][j
                                            - 1] == myBoard[i - 2][j - 2]
                                            && myBoard[i - 1][j - 1] != null)
                                    {
                                        if (myBoard[i][j] == myBoard[i - 1][j
                                                - 1])
                                        {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;

        }
        return false;
    }

    /**
     * Count the horizontal length for next piece. Boundaries solved.
     * 
     * @param col
     * @param type
     * @return horizontal length
     */
    private int countHorizontalLengthIfPiecePlaced(int col, PieceType type)
    {
        int result = 0;
        int tempRow = 6;

        for (int i = myNumRows - 1; i >= 0; i--)
        {
            if (myBoard[i][col] == null)
            {
                tempRow = i;
                i = -1;
            }
        }
        if (tempRow != myNumRows)
        {
            if (col > 0)
            {
                for (int j = col - 1; j >= 0; j--)
                {
                    if (myBoard[tempRow][j] == type)
                    {
                        result++;
                    } else
                    {
                        j = -1;
                    }
                }
            } else if (col < 6)
            {
                for (int k = col + 1; k < myNumColumns; k++)
                {
                    if (myBoard[tempRow][k] == type)
                    {
                        result++;
                    } else
                    {
                        k = 100;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Count the vertical length for next piece. Boundaries solved.
     * 
     * @param col
     * @param type
     * @return vertical length
     */
    private int countVerticalLengthIfPiecePlaced(int col, PieceType type)
    {
        int result = 0;
        int tempRow = 6;

        for (int i = myNumRows - 1; i >= 0; i--)
        {
            if (myBoard[i][col] == null)
            {
                tempRow = i;
                i = -1;
            }
        }
        if (tempRow < myNumRows - 1 && tempRow > -1)
        {
            for (int j = tempRow + 1; j < myNumRows; j++)
            {
                if (myBoard[j][col] == type)
                {
                    result++;
                } else
                {
                    j = myNumRows;
                }
            }
        }
        return result;
    }

    /**
     * Count the diagonal length for next piece. Boundaries solved.
     * 
     * @param col
     * @param type
     * @return diagonal length
     */
    private int countDiagonalLengthIfPiecePlaced(int col, PieceType type)
    {
        int result = 0;
        int tempRow = 6;

        for (int i = myNumRows - 1; i >= 0; i--)
        {
            if (myBoard[i][col] == null)
            {
                tempRow = i;
                i = -1;
            }
        }
        if (tempRow != 6)
        {

            if (col != 0 && tempRow != myNumRows - 1)
            {
                for (int i = tempRow + 1, j = col - 1; i < myNumRows
                        && j >= 0; i++, j--)
                {
                    if (myBoard[i][j] == type)
                    {
                        result++;
                    } else
                    {
                        j = -1;
                    }
                }
            }
            if (col != myNumColumns - 1 && tempRow != 0)
            {
                for (int i = tempRow - 1, j = col + 1; i >= 0
                        && j < myNumColumns; i--, j++)
                {
                    if (myBoard[i][j] == type)
                    {
                        result++;
                    } else
                    {
                        i = -1;
                    }
                }
            }
            if (col != myNumColumns - 1 && tempRow != myNumRows - 1)
            {
                for (int i = tempRow + 1, j = col + 1; i < myNumRows
                        && j < myNumColumns; i++, j++)
                {
                    if (myBoard[i][j] == type)
                    {
                        result++;
                    } else
                    {
                        i = myNumRows;
                    }
                }
            }

            if (col != 0 && tempRow != 0)
            {
                for (int i = tempRow - 1, j = col - 1; i >= 0
                        && j >= 0; i--, j--)
                {
                    if (myBoard[i][j] == type)
                    {
                        result++;
                    } else
                    {
                        i = -1;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Find the win begin points in horizontal direction.
     * 
     * @return begin point of win(horizontal)
     */
    private Point FindWinBeginHorizontal()
    {
        int result = 0;
        int col = myWinEnd.x;
        int row = myWinEnd.y;
        PieceType type = myBoard[row][col];
        Point tempP = new Point(0, 0);

        if (col > 0)
        {
            for (int j = col - 1; j >= 0; j--)
            {
                if (myBoard[row][j] == type)
                {
                    result++;
                    tempP = new Point(j, row);
                } else
                {
                    j = -1;
                }
            }

            if (result == myWinLength - 1)
            {
                return tempP;
            } else
            {
                result = 0;
            }
        }

        if (col < myNumColumns - 1)
        {
            for (int k = col + 1; k < myNumColumns; k++)
            {
                if (myBoard[row][k] == type)
                {
                    result++;
                    tempP = new Point(k, row);
                } else
                {
                    k = 100;
                }
            }
            if (result == myWinLength - 1)
            {
                return tempP;
            } else
            {
                result = 0;
            }
        }
        return null;
    }

    /**
     * Find the win begin points in vertical direction
     * 
     * @return begin point of win(vertical)
     */
    private Point FindWinBeginVertical()
    {
        int result = 0;
        int col = myWinEnd.x;
        int row = myWinEnd.y;
        PieceType type = myBoard[row][col];
        Point tempP = new Point(0, 0);

        if (row < myNumRows - 1)
        {
            for (int j = row + 1; j < myNumRows; j++)
            {
                if (myBoard[j][col] == type)
                {
                    result++;
                    tempP = new Point(col, j);
                } else
                {
                    j = myNumRows;
                }
            }
        }
        if (result == myWinLength - 1)
        {
            return tempP;
        }
        return null;
    }

    /**
     * Find the win begin points in diagonal direction
     * 
     * @return begin point of win(diagonal)
     */
    private Point FindWinBeginDiagonal()
    {
        int result = 0;
        int col = myWinEnd.x;
        int row = myWinEnd.y;
        PieceType type = myBoard[row][col];
        Point tempP = new Point(0, 0);

        if (col != 0 && row != myNumRows - 1)
        {
            for (int i = row + 1, j = col - 1; i < myNumRows
                    && j >= 0; i++, j--)
            {
                if (myBoard[i][j] == type)
                {
                    result++;
                    tempP = new Point(j, i);
                } else
                {
                    j = -1;
                }
            }
        }

        if (result == myWinLength - 1)
        {
            return tempP;
        } else
        {
            result = 0;
        }

        if (col != myNumColumns - 1 && row != 0)
        {
            for (int i = row - 1, j = col + 1; i >= 0
                    && j < myNumColumns; i--, j++)
            {
                if (myBoard[i][j] == type)
                {
                    result++;
                    tempP = new Point(j, i);
                } else
                {
                    i = -1;
                }
            }
        }

        if (result == myWinLength - 1)
        {
            return tempP;
        } else
        {
            result = 0;
        }

        if (col != myNumColumns - 1 && row != myNumRows - 1)
        {
            for (int i = row + 1, j = col + 1; i < myNumRows
                    && j < myNumColumns; i++, j++)
            {
                if (myBoard[i][j] == type)
                {
                    result++;
                    tempP = new Point(j, i);
                } else
                {
                    i = myNumRows;
                }
            }
        }

        if (result == myWinLength - 1)
        {
            return tempP;
        } else
        {
            result = 0;
        }

        if (col != 0 && row != 0)
        {
            for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--)
            {
                if (myBoard[i][j] == type)
                {
                    result++;
                    tempP = new Point(j, i);
                } else
                {
                    i = -1;
                }
            }
        }

        if (result == myWinLength - 1)
        {
            return tempP;
        } else
        {
            result = 0;
        }
        return null;
    }

    /**
     * Get the win begin point after running three "findWinBegin" methods.
     */
    private void findWinBeginPoints()
    {
        if (this.FindWinBeginHorizontal() != null)
        {
            myWinBegin = this.FindWinBeginHorizontal();
        } else if (this.FindWinBeginVertical() != null)
        {
            myWinBegin = this.FindWinBeginVertical();
        } else if (this.FindWinBeginDiagonal() != null)
        {
            myWinBegin = this.FindWinBeginDiagonal();
        }
    }

    public Point getWinBegin()
    {
        return myWinBegin;
    }

    public Point getWinEnd()
    {
        return myWinEnd;
    }

    public Point getLastPoint()
    {
        return myLastPoint;
    }

    /**
     * get piece on board
     * 
     * @param point
     * @return piece on board
     */
    public PieceType getPieceOnBoard(Point point)
    {
        return myBoard[point.y][point.x];
    }

    public PieceType[][] getBoard()
    {
        return myBoard;
    }

    /**
     * Check the board if full or not
     * 
     * @return board status
     */
    public boolean isBoardFull()
    {
        for (int i = myNumRows - 1; i >= 0; i--)
        {
            for (int j = 0; j < myNumColumns; j++)
            {
                if (myBoard[i][j] == null)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public ComputerPlayer getComputerPlayer()
    {
        return myComputer;
    }
    /**
     * Check the column if full or not
     * 
     * @param col
     * @return column status
     */
    public boolean isColumnFull(int col)
    {
       // int length = 0;
        for (int i = myNumRows - 1; i >= 0; i--)
        {
            if (myBoard[i][col] == null)
            {
                return false;
            }
        }
        
        return true;
    }

    public boolean getIsAWin()
    {
        return myIsAWin;
    }
    /**
     * Check if the piece is placed or not
     * 
     * @return piece status
     */
    public boolean checkAllNull()
    {
        for (int i = myNumRows - 1; i >= 0; i--)
        {
            for (int j = 0; j < myNumColumns; j++)
            {
                if (myBoard[i][j] != null)
                {
                    return false;
                }
            }
        }
        return true;
    }
}