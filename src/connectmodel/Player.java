package connectmodel;

/**
 * Player class of the connect 4 game.
 * 
 * @author Yizheng Wang
 *
 */
public class Player
{
    public static final String DEFAULT_NAME = "JohnCena";
    private String             myName;
    private int                myNumWins = 0;
    private PieceType          myPieceType;

    /**
     * Default Player constructor
     */
    public Player()
    {
        myName = DEFAULT_NAME;
    }

    /**
     * Player constructor
     * 
     * @param name
     * @return
     */
    public Player(String name, PieceType type)
    {
        myName = name;
        myPieceType = type;
    }

    /**
     * Check if the name is valid
     * 
     * @return
     */
    private boolean nameStatus()
    {
        if (myName != "")
        {
            String[] searchChars =
            {"!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "=",
            "+", "[", "{", "]", "}", "|", ";", ":", "'", ",", "<", ".", ">",
            "/", "?", " "};
            for (int i = 0; i < searchChars.length; i++)
            {
                if (myName.contains(searchChars[i]) == true)
                {
                    return false;
                }
            }

        } else
        {
            return false;
        }
        return true;
    }

    /**
     * Increment score
     */
    public void incrementScore()
    {
        myNumWins += 1;

    }

    public PieceType getPieceType()
    {
        return myPieceType;
    }

    /**
     * Get player's name. If the name status is false, return the default name.
     * @return
     */
    public String getName()
    {
        if (nameStatus() == false)
        {
            return DEFAULT_NAME;
        }

        return myName;

    }

    public int getNumWins()
    {
        return myNumWins;
    }

    public int getScore()
    {
        return myNumWins;
    }
}